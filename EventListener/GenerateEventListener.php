<?php

namespace AveSystems\ClientBusBundle\EventListener;

use AveSystems\ClientBusBundle\Helper\ClientBusConstants;
use AveSystems\ClientBusBundle\Service\QueueHelperInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Jett\JSONEntitySerializerBundle\Service\SerializerInterface;

/**
 * Generates events on changes in global entities
 * and writes them to queue for service bus to read.
 *
 * Class GenerateEventListener
 */
class GenerateEventListener
{
    /**
     * Entity FQCN -> GEA (Global Entity Alias).
     *
     * @var array
     */
    protected $_info;

    protected $_serializer;

    protected $_queue;

    protected $_enabled;

    protected $_sendQueue;

    public function __construct($info, $enabled, $sendQueue)
    {
        $this->_info = array_flip($info);
        $this->_enabled = $enabled;
        $this->_sendQueue = $sendQueue;
    }

    public function setServices(SerializerInterface $serializer, QueueHelperInterface $queue)
    {
        $this->_serializer = $serializer;
        $this->_queue = $queue;
    }

    public function setEnabled($enabled)
    {
        $this->_enabled = $enabled;
    }

    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        if (!$this->_enabled) {
            return;
        }
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();
        $events = [];
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if ($event = $this->generateEvent($entity, ClientBusConstants::EVENT_CRUD_CREATE)) {
                $events[] = $event;
            }
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($event = $this->generateEvent($entity, ClientBusConstants::EVENT_CRUD_UPDATE)) {
                $events[] = $event;
            }
        }

        $events = array_merge($events, $this->generateDeleteEvents($uow->getScheduledEntityDeletions()));

        if (count($events)) {
            $event = $this->createTransaction($events);
            $this->_queue->send(json_encode($event), $this->_sendQueue);
        }
    }

    protected function getEventId($event)
    {
        return sha1(microtime(true).json_encode($event));
    }

    protected function generateEvent($entity, $type)
    {
        $class = get_class($entity);
        if (!isset($this->_info[$class])) {
            return false;
        }
        $eventData = $this->_serializer->toPureObject($entity, 'all');
        $event = new \stdClass();
        $event->type = ClientBusConstants::EVENT_TYPE_CRUD;
        $event->name = $this->_info[$class];
        $event->data = new \stdClass();
        $event->data->action = $type;
        $event->data->data = $eventData;
        $event->id = $this->getEventId($event);

        return $event;
    }

    protected function generateDeleteEvents($entities)
    {
        $deletion = [];
        $events = [];

        foreach ($entities as $entity) {
            $class = get_class($entity);
            if (!isset($this->_info[$class])) {
                continue;
            }
            if (!isset($deletion[$this->_info[$class]])) {
                $deletion[$this->_info[$class]] = [];
            }
            $deletion[$this->_info[$class]][] = $entity->getId();
        }

        foreach ($deletion as $class => $delete) {
            $event = new \stdClass();
            $event->type = ClientBusConstants::EVENT_TYPE_CRUD;
            $event->name = $class;
            $event->data = new \stdClass();
            $event->data->action = ClientBusConstants::EVENT_CRUD_DELETE;
            $event->data->data = $delete;
            $event->id = $this->getEventId($event);
            $events[] = $event;
        }

        return $events;
    }

    protected function createTransaction($events)
    {
        $event = new \stdClass();
        $event->type = ClientBusConstants::EVENT_TYPE_TRANSACTION;
        $event->name = 'transaction';
        $event->data = $events;
        $event->id = $this->getEventId($events);

        return $event;
    }
}
