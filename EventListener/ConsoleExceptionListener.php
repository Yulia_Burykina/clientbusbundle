<?php

namespace AveSystems\ClientBusBundle\EventListener;

use AveSystems\ClientBusBundle\Command\IntegrationBusStartCommand;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Event\ConsoleErrorEvent;
use Symfony\Component\Console\Event\ConsoleExceptionEvent;

/**
 * Listens to IntegrationBusStartCommand exceptions
 * and logs them to 'ave_client_bus_command' channel.
 *
 * Class ConsoleExceptionListener
 */
class ConsoleExceptionListener
{
    private $logger;

    public function __construct(LoggerInterface $logger = null)
    {
        $this->logger = $logger;
    }

    public function onConsoleException($event)
    {
        if (!$this->logger) {
            return;
        }
        $command = $event->getCommand();
        if (IntegrationBusStartCommand::NAME !== $command->getName()) {
            return;
        }
        $exception = null;
        if ($event instanceof ConsoleErrorEvent) {
            $exception = $event->getError();
        } elseif ($event instanceof ConsoleExceptionEvent) {
            $exception = $event->getException();
        } else {
            return;
        }

        $message = sprintf(
            '%s: %s (uncaught exception) at %s line %s',
            get_class($exception),
            $exception->getMessage(),
            $exception->getFile(),
            $exception->getLine()
        );

        $this->logger->error($message, ['exception' => $exception]);
    }
}
