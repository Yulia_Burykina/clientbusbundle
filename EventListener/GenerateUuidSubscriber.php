<?php

namespace AveSystems\ClientBusBundle\EventListener;

use AveSystems\ClientBusBundle\Interfaces\GlobalEntity;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Id\UuidGenerator;

/**
 * This subscriber is used to generate Uuid for new
 * global entities that don't have it preset.
 *
 * Class GenerateUuidSubscriber
 */
class GenerateUuidSubscriber implements EventSubscriber
{
    /** @var array|bool %ave_client_bus.entities% */
    protected $_info;

    public function __construct(array $info)
    {
        $this->_info = array_flip($info);
    }

    public function getSubscribedEvents()
    {
        return [
            'prePersist',
        ];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        /** @var GlobalEntity $entity */
        $entity = $args->getEntity();
        $class = ClassUtils::getClass($entity);
        $isGlobal = isset($this->_info[$class]);
        if (!$isGlobal) {
            return;
        }
        $em = $args->getEntityManager();
        if ($entity->getId()) {
            return;
        }
        $generator = new UuidGenerator();
        $value = $generator->generate($em, $entity);
        $entity->setId($value);
    }
}
