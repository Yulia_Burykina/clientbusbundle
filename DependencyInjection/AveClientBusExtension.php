<?php

namespace AveSystems\ClientBusBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @see http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class AveClientBusExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $container->setParameter('ave_client_bus.service_bus_address', $config['service_bus_address']);
        $container->setParameter('ave_client_bus.service_bus_port', $config['service_bus_port']);
        $container->setParameter('ave_client_bus.entities', $config['entities']);
        $container->setParameter('ave_client_bus.client', $config['client']);
        $container->setParameter('ave_client_bus.rabbitmq.connection', $config['rabbitmq']['connection']);
        $container->setParameter('ave_client_bus.rabbitmq.queues', $config['rabbitmq']['queues']);
        $container->setParameter('ave_client_bus.rabbitmq.queues.input_queue', $config['rabbitmq']['queues']['input_queue']);
        $container->setParameter('ave_client_bus.rabbitmq.queues.output_queue', $config['rabbitmq']['queues']['output_queue']);
        $container->setParameter('ave_client_bus.generate_event_listener_enabled', $config['generate_event_listener_enabled']);
    }
}
