<?php

namespace AveSystems\ClientBusBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class EventExecutorPass.
 */
class EventExecutorPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('ave_client_bus.event_executor')) {
            return;
        }

        $definition = $container->getDefinition('ave_client_bus.event_executor');

        $taggedPreprocessors = $container->findTaggedServiceIds('ave_client_bus.event_preprocessor');
        uasort($taggedPreprocessors, function ($a, $b) {
            return ($a['priority'] ?? 0) - ($b['priority'] ?? 0);
        });

        foreach ($taggedPreprocessors as $id => $attributes) {
            $definition->addMethodCall('addPreprocessor', [new Reference($id)]);
        }
    }
}
