<?php

namespace AveSystems\ClientBusBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ave_client_bus');

        $this->addBasicConfig($rootNode);
        $this->addClientConfig($rootNode);
        $this->addRabbitMQConfig($rootNode);

        return $treeBuilder;
    }

    private function addBasicConfig(ArrayNodeDefinition $rootNode)
    {
        $rootNode->children()
                    ->booleanNode('generate_event_listener_enabled')
                        ->info('Enable sending events about data changes to integration bus')
                        ->defaultValue(true)
                    ->end()
                    ->scalarNode('service_bus_address')
                        ->isRequired()
                        ->cannotBeEmpty()
                        ->info('Service bus IP address or host name')
                    ->end()
                    ->integerNode('service_bus_port')
                        ->isRequired()
                        ->info('Servce bus port to connect')
                    ->end()
                    ->arrayNode('entities')
                        ->isRequired()
                        ->requiresAtLeastOneElement()
                        ->useAttributeAsKey('name')
                        ->info('Entity class map. Global entity alias (GEA) -> Entity FQCN')
                        ->prototype('scalar')->end()
                    ->end()
                ->end();
    }

    private function addClientConfig(ArrayNodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->arrayNode('client')
                    ->isRequired()
                    ->cannotBeEmpty()
                    ->info('Bus client data')
                    ->children()
                        ->scalarNode('random_id')
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->info('Client id according to OAuth2 standart')
                        ->end()
                        ->scalarNode('secret')
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->info('Client secret according to OAuth2 standart')
                        ->end()
                        ->scalarNode('name')
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->info('Bus client name - human readable')
                        ->end()
                        ->arrayNode('subscriptions')
                            ->info('Client subscriptions')
                            ->defaultValue([])
                            ->prototype('array')
                                ->children()
                                    ->integerNode('type')
                                        ->isRequired()
                                        ->min(0)->max(3)
                                        ->info('CRUD = 0, REQUEST = 1, CUSTOM = 2, TRANSACTION = 3')
                                        ->end()
                                    ->scalarNode('name')
                                        ->isRequired()
                                        ->info('Event name or entity global alias')
                                    ->end()
                                    ->arrayNode('data')
                                        ->children()
                                            ->scalarNode('map')->info('JSON - example object')->end()
                                            ->booleanNode('create')
                                                ->info('Set to true if you want to receive "create" events about this entity')
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()

            ->end();
    }

    private function addRabbitMQConfig(ArrayNodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->arrayNode('rabbitmq')
                    ->isRequired()
                    ->cannotBeEmpty()
                    ->children()
                        ->arrayNode('connection')
                            ->children()
                                ->scalarNode('host')->isRequired()->cannotBeEmpty()->end()
                                ->integerNode('port')->isRequired()->end()
                                ->scalarNode('login')->isRequired()->end()
                                ->scalarNode('pwd')->isRequired()->end()
                            ->end()
                        ->end()
                        ->arrayNode('queues')
                            ->children()
                                ->scalarNode('input_queue')->isRequired()->cannotBeEmpty()->end()
                                ->scalarNode('output_queue')->isRequired()->cannotBeEmpty()->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }
}
