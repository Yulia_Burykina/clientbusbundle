<?php

namespace AveSystems\ClientBusBundle\Helper;

/**
 * Class ClientBusConstants.
 */
class ClientBusConstants
{
    const EVENT_TYPE_SYNC = 0;
    const EVENT_TYPE_ASYNC = 1;
    const EVENT_TYPE_MASTER_SYNC = 2;

    const EVENT_TYPE_CRUD = 0;
    const EVENT_TYPE_REQUEST = 1;
    const EVENT_TYPE_CUSTOM = 2;
    const EVENT_TYPE_TRANSACTION = 3;

    const EVENT_CRUD_CREATE = 0;
    const EVENT_CRUD_UPDATE = 1;
    const EVENT_CRUD_DELETE = 2;

    const OBJECT_RESOLVER_LOAD_MODE = 4;
}
