# AveClientBusBundle

## Overview

Symfony 3 bundle that provides data integrity for system of applications with shared data. This bundle is installed on each application in the system. It communicates with Service Bus written in Node.js, which maintains shared data integrity by receiving messages (they are called 'events' in this system) containing "data patches" from applications, routing these messages and controlling the process of applying these patches.    

This bundle ensures integrity and consistency of Doctrine2 entities that are shared between multiple applications, the so-called global entities. It uses RabbitMQ to exchange messages about data modification with Service Bus. 

To provide data consistency AveClientBusBundle exchanges state messages with Service Bus via TCP sockets.

It supports multiple levels of data consistency: for a strong consistency it uses database transactions to apply data changes received from Service Bus. It commits (or rolls back) those changes only after receiving the corresponding message on the socket, thus making possible for Service Bus to control data coherency on all applications. For lower level of consistency it just sends "state insync" socket message to Service Bus if the changes could not be applied.

## Installation

To install this Bundle add the following entry to your `composer.json`:
```
"avesystems/client-bus-bundle": "^1.0"
```
Then add the bundle to your `AppKernel.php` file:
```
// in AppKernel::registerBundles()
$bundles = array(
    // ...
    new AveSystems\ClientBusBundle\AveClientBusBundle(),
    // ...
);
```

## Configuration

This bundle requires the following configuration in your `config.yml`:
```
ave_client_bus:
    # whether changes in global entites made in this application
    # should be sent as a RabbitMQ messages ('events') to Service Bus;
    # defaults to true
    generate_event_listener_enabled: true
    
    # Service Bus url config
    service_bus_address: your_service_bus_scheme_and_host
    service_bus_port: your_service_bus_port
    
    # map of global entities aliases; these aliases are used
    # in events and should be the same for every 
    # application that uses this bundle
    entities:
        user: YourAppNamespace\Entity\User
        role: YourAppNamespace\Entity\Role
        
    # bundle sends this information about the application to Service Bus
    client:
        # authentication info for the application: id, secret, name 
        random_id: test
        secret: test
        name: test
        # description of global entities that the application wants
        # to receive events about from Service Bus
        subscriptions:
              # type of event that the application is  interested in
              # receiving: 0 - CRUD, 1 - REQUEST, 2 - CUSTOM, 3 - TRANSACTION;
              # currently only CRUD and TRANSACTION types are implemented
            - type: 0
              # entity alias
              name: user
              data:
                  # whether the app should receive CRUD events
                  # of type CREATE for that entity
                  # (CRUD event types are CREATE, UPDATE, DELETE)
                  create: false
                  # sample object representing which properties of the 
                  # entity the app is interested in; if the value of 
                  # this field is 'all', the entity will contain every
                  # property, relation properties will be represented either as object 
                  # containing only 'id' property or collections of such objects
                  map: >
                      {
                          "id": "1",
                          "firstName": "1",
                          "lastName": "1",
                          "email": "1",
                          "role": {
                              "id": "1"
                          }
                      }
            - type: 0
              name: role
              data:
                  create: true
                  map: all
                  
    # rabbitMQ config             
    rabbitmq:
        connection:
            host: localhost
            port: 5762
            login: ~
            pwd: ~
        queues:
            # rabbitMQ queue for receiving messages from Service Bus
            input_queue: test_input
            # rabbitMQ queue for sending messages to Service Bus
            output_queue: test_output
```

## Usage

This bundle receives RabbitMQ messages ('events') about changes in configured global entities and applies these changes to application database. It also sends events about changes in global entities that happened inside the application, so the other applications in a system could apply those changes. To use it, you must:

* configure the bundle;

* all the global entities configured must implement `AveSystems\ClientBusBundle\Interfaces\GlobalEntity` interface, i.e. they must implement `getId()` and `setId($id)` methods;

* configure `integration:bus:start` as system daemon (command has no options);

Some services from this bundle can be rewritten to adjust them to particular applications.

To rewrite `ave_client_bus.event_patcher` service that is in charge of applying incoming events with data patches to database, simply override its definition (in your class you must implement `AveSystems\ClientBusBundle\Service\EventPatcherInterface`).

You can also add preprocessors that can modify the event before it is passed to the patcher. You do that by implementing `AveSystems\ClientBusBundle\Service\EventPreprocessorInterface` in your preprocessors, declaring them as services and tagging these services with `ave_client_bus.event_preprocessor`.

## Exception Logging

Exceptions thrown by the command are logged into monolog channel named 'ave_client_bus_command'; exceptions thrown by symfony patcher are logged into monolog channel named 'ave_client_bus_patcher';
you can direct those channels to different places/files as shown in symfony documentation (https://symfony.com/doc/current/logging/channels_handlers.html).
