<?php

namespace AveSystems\ClientBusBundle;

use AveSystems\ClientBusBundle\DependencyInjection\Compiler\EventExecutorPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AveClientBusBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new EventExecutorPass());
    }
}
