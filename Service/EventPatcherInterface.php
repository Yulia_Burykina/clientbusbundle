<?php

namespace AveSystems\ClientBusBundle\Service;

interface EventPatcherInterface
{
    public function apply($evt, $asTransaction);

    public function commit($evt);

    public function rollback($evt);
}
