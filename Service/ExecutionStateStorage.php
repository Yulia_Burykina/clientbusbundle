<?php

namespace AveSystems\ClientBusBundle\Service;

use AveSystems\ClientBusBundle\Interfaces\ExecutionState;

/**
 * This service is used to store the state of execution
 * of the event being executed by IntegrationBusStartCommand
 * in a file. This allows to resume the execution
 * of event from the last "saving point"
 * in case daemon running the command stops unexpectedly.
 *
 * Class ExecutionStateStorage
 */
class ExecutionStateStorage implements ExecutionStateStorageInterface
{
    protected $pathToLog;

    public function __construct($pathToLog)
    {
        $this->pathToLog = $pathToLog;
    }

    public function get()
    {
        if (!file_exists($this->pathToLog)) {
            return false;
        }
        $contents = file_get_contents($this->pathToLog);

        return @json_decode($contents) ?: null;
    }

    public function set(ExecutionState $execState = null)
    {
        $contents = json_encode($execState);
        if (!$contents) {
            throw new \Exception('trying to set invalid execution state');
        }
        $res = file_put_contents($this->pathToLog, $contents);
        if (false === $res) {
            throw new \Exception('error while trying to write execution state');
        }
    }
}
