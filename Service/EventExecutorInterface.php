<?php

namespace AveSystems\ClientBusBundle\Service;

interface EventExecutorInterface
{
    public function applyEvent($evt, $asTransaction = false);

    public function commitEvent($evt);

    public function rollbackEvent($evt);
}
