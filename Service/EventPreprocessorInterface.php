<?php

namespace AveSystems\ClientBusBundle\Service;

/**
 * An interface for event preprocessors - services that
 * modify the event before it is passed to the patcher.
 *
 * Interface EventPreprocessorInterface
 */
interface EventPreprocessorInterface
{
    public function preProcess($evt);
}
