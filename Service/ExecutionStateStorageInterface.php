<?php

namespace AveSystems\ClientBusBundle\Service;

use AveSystems\ClientBusBundle\Interfaces\ExecutionState;

interface ExecutionStateStorageInterface
{
    /**
     * @return bool|ExecutionState
     */
    public function get();

    public function set(ExecutionState $state = null);
}
