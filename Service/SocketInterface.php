<?php

namespace AveSystems\ClientBusBundle\Service;

interface SocketInterface
{
    /**
     * Creates necessary resources.
     *
     * @return mixed
     */
    public function create();

    /**
     * Authenticates the client in the service bus.
     *
     * @param array|null $credentials
     *
     * @return array - [boolean - success/failure, string - message]
     */
    public function authenticate($credentials = null): array;

    /**
     * Writes a message to socket after serializing it to JSON.
     *
     * @param mixed $msg
     */
    public function write($msg);

    /**
     * Reads message from socket and unserializes it.
     *
     * @return mixed
     */
    public function read();

    /**
     * Shortcut for sending a message and receiving a response.
     *
     * @param mixed $msg
     *
     * @return mixed
     */
    public function sendBack($msg);

    /**
     * Shortcut for authenticating a client, sending a message and receiving a response.
     *
     * @param array|null $credentials
     * @param mixed      $msg
     *
     * @return mixed
     */
    public function authSendBack($credentials, $msg);
}
