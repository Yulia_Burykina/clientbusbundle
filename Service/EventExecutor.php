<?php

namespace AveSystems\ClientBusBundle\Service;

/**
 * Class EventExecutor - executes event.
 */
class EventExecutor implements EventExecutorInterface
{
    /** @var EventPatcherInterface */
    private $patcher;

    /** @var EventPreprocessorInterface[] */
    private $preprocessors = [];

    public function __construct(EventPatcherInterface $patcher)
    {
        $this->patcher = $patcher;
    }

    public function addPreprocessor(EventPreprocessorInterface $preprocessor)
    {
        $this->preprocessors[] = $preprocessor;
    }

    public function applyEvent($evt, $asTransaction = false)
    {
        foreach ($this->preprocessors as $pre) {
            $evt = $pre->preProcess($evt);
        }

        return $this->patcher->apply($evt, $asTransaction);
    }

    public function commitEvent($evt)
    {
        return $this->patcher->commit($evt);
    }

    public function rollbackEvent($evt)
    {
        return $this->patcher->rollback($evt);
    }
}
