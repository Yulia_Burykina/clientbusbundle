<?php

namespace AveSystems\ClientBusBundle\Service;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Adapter for php-amqplib.
 *
 * Class QueueHelper
 */
class QueueHelper
{
    private $rabbitMQConnParams;

    private $connection;

    private $channel;

    public function __construct($rabbitMQConnParams)
    {
        $this->rabbitMQConnParams = $rabbitMQConnParams;

        $this->connection = new AMQPStreamConnection(
            $this->rabbitMQConnParams['host'],
            $this->rabbitMQConnParams['port'],
            $this->rabbitMQConnParams['login'],
            $this->rabbitMQConnParams['pwd']
        );
        $this->channel = $this->connection->channel();
    }

    public function __destruct()
    {
        $this->channel->close();
        $this->connection->close();
    }

    /**
     * @param callable $callback
     * @param string   $queue
     *
     * @throws \Exception
     */
    public function listen(callable $callback, $queue)
    {
        $this->channel->queue_declare($queue, false, true, false, false);

        $this->channel->basic_consume($queue, '', false, false, false, false, $callback);

        while (count($this->channel->callbacks)) {
            $this->channel->wait();
        }
    }

    public function acknowledge(AMQPMessage $msg)
    {
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    }

    /**
     * @param string $msg   - JSON encoded message
     * @param string $queue
     *
     * @throws \Exception
     */
    public function send($msg, $queue)
    {
        $msg = new AMQPMessage($msg, ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);

        $this->channel->queue_declare($queue, false, true, false, false);

        $this->channel->basic_publish($msg, '', $queue);
    }
}
