<?php

namespace AveSystems\ClientBusBundle\Service;

use AveSystems\ClientBusBundle\Helper\ClientBusConstants;

/**
 * Event preprocessor that validates the event.
 *
 * Class EventValidationPreprocessor
 */
class EventValidationPreprocessor implements EventPreprocessorInterface
{
    public function preProcess($evt)
    {
        return $this->validate($evt);
    }

    protected function validate($evt)
    {
        if (!$evt || !isset($evt->syncType) ||
                !in_array($evt->syncType, [
                    ClientBusConstants::EVENT_TYPE_SYNC,
                    ClientBusConstants::EVENT_TYPE_ASYNC,
                    ClientBusConstants::EVENT_TYPE_MASTER_SYNC,
                ], true)) {
            throw new \Exception('Event should have syncType defined');
        }

        return $evt;
    }
}
