<?php

namespace AveSystems\ClientBusBundle\Service;

use AveSystems\ClientBusBundle\Interfaces\State;
use AveSystems\ClientBusBundle\Interfaces\StateMessage;

/**
 * Class Socket.
 */
class Socket implements SocketInterface
{
    const SEP = "\r\n";
    private $socket;
    private $address;
    private $port;

    private $errno = 0;
    private $errstr = '';

    private $connected = false;

    private static $auth_errors = [
        State::ERROR => 'service internal error',
        State::INSYNC => 'service insync',
        State::NOT_IMPLEMENTED => 'not implemented',
        State::NOT_AUTH => 'wrong credentials',
        State::OFFLINE => 'service is offline',
    ];

    public function __construct($address, $port)
    {
        $this->address = $address;
        $this->port = $port;
    }

    public function __destruct()
    {
        $this->stop();
    }

    public function create()
    {
        if ($this->socket) {
            return;
        }
        $this->socket = stream_socket_client(
            'tcp://'.$this->address.':'.$this->port,
            $this->errno,
            $this->errstr
        );
        if (false === $this->socket) {
            throw new \Exception(
                sprintf('failed to create socket: %s', $this->errstr)
            );
        }
        $this->connected = true;
    }

    public function authenticate($credentials = null): array
    {
        if (empty($credentials)) {
            throw new \InvalidArgumentException('Credentials are not optional here.');
        }
        $this->write($credentials);
        /** @var StateMessage $resp */
        $resp = $this->read();

        if (!isset($resp->status) || !is_numeric($resp->status)) {
            return [false, 'Invalid response object from ESB service.'];
        }
        $state = (int) $resp->status;
        if (State::OK !== $state) {
            return [false, self::$auth_errors[$state]];
        }

        return [true, null];
    }

    public function write($msg)
    {
        $message = json_encode($msg);
        $message = 'Length: '.strlen($message).self::SEP.self::SEP.$message.self::SEP;
        $res = fwrite($this->socket, $message);
        if (false === $res) {
            throw new \Exception('failed to write in socket.');
        }
    }

    /**
     * Reads from socket
     * Returns false in case of error.
     *
     * @return bool|string
     */
    public function read()
    {
        $buf = '';
        $length = 0;
        $isContent = false;
        do {
            if (!$isContent) {
                $string = fgets($this->socket, 1024);
                if (false === $string) {
                    return false;
                }
                if ('' === $string) {
                    return '';
                }
                if (preg_match("/Length:\s(\d+)/", $string, $matches)) {
                    $length = (int) $matches[1];
                }
                $buf .= $string;
                if (substr($buf, -4) === self::SEP.self::SEP) {
                    $isContent = true;
                }
            } else {
                $content = fread($this->socket, $length + strlen(self::SEP));
                if ($content && (strlen($content) - strlen(self::SEP)) === $length) {
                    $content = trim($content);
                    $msg = json_decode($content);

                    return $msg ?? $content;
                }

                return false;
            }
        } while ($this->connected);

        return false;
    }

    public function authSendBack($credentials, $msg)
    {
        $this->authenticate($credentials);
        $this->write($msg);

        return $this->read();
    }

    public function sendBack($msg)
    {
        $this->write($msg);

        return $this->read();
    }

    public function stop()
    {
        if ($this->connected) {
            fclose($this->socket);
            $this->connected = false;
        }
    }
}
