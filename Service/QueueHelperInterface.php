<?php

namespace AveSystems\ClientBusBundle\Service;

interface QueueHelperInterface
{
    public function listen(callable $callback, $queue);

    public function acknowledge($msg);

    public function send($msg, $queue);
}
