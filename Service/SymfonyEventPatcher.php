<?php

namespace AveSystems\ClientBusBundle\Service;

use AveSystems\ClientBusBundle\Helper\ClientBusConstants;
use AveSystems\ObjectResolverBundle\ObjectResolver;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Applies event to database using Doctrine EntityManager and Symfony Validator.
 *
 * Class SymfonyEventPatcher
 */
class SymfonyEventPatcher implements EventPatcherInterface
{
    /** @var EntityManagerInterface */
    private $em;
    /** @var ObjectResolver */
    private $resolver;
    /** @var string[] */
    private $classMap;
    /** @var LoggerInterface */
    private $logger;
    /** @var ValidatorInterface */
    private $validator;

    public function __construct(
        EntityManagerInterface $em, ObjectResolver $resolver, array $classMap, ValidatorInterface $validator, LoggerInterface $logger
    ) {
        $this->em = $em;
        $this->resolver = $resolver;
        $this->classMap = $classMap;
        $this->validator = $validator;
        $this->logger = $logger;
    }

    /**
     * Method applies event to DB via Entity Manager.
     * If $asTransaction set to true it starts transaction but doesn't commit it
     * (you need to commit or rollback it explicitly via corresponding methods).
     *
     * @param $evt
     * @param $asTransaction
     *
     * @return bool
     */
    public function apply($evt, $asTransaction)
    {
        $transType = ClientBusConstants::EVENT_TYPE_TRANSACTION === $evt->type;
        $startTransaction = $asTransaction || $transType;

        try {
            if ($startTransaction) {
                $this->em->getConnection()->beginTransaction();
            }

            if (!$transType) {
                return $this->applySimple($evt);
            }
            foreach ($evt->data as $subEvt) {
                //a transaction cannot be nested into another transaction
                $res = $this->applySimple($subEvt);
                if (!$res) {
                    $this->em->getConnection()->rollBack();

                    return false;
                }
            }
            if (!$asTransaction) {
                $this->em->getConnection()->commit();
            }

            return true;
        } catch (\Exception $e) {
            if ($startTransaction) {
                $this->em->getConnection()->rollBack();
            }
            $this->logException($e, $evt);

            return false;
        }
    }

    /**
     * @param $evt
     *
     * @return bool
     */
    public function commit($evt)
    {
        try {
            $this->em->getConnection()->commit();

            return true;
            // @codeCoverageIgnoreStart
        } catch (\Exception $e) {
            $this->logException($e, $evt);

            return false;
        }
        // @codeCoverageIgnoreEnd
    }

    public function rollback($evt)
    {
        try {
            $this->em->getConnection()->rollBack();

            return true;
            // @codeCoverageIgnoreStart
        } catch (\Exception $e) {
            $this->logException($e, $evt);

            return false;
        }
        // @codeCoverageIgnoreEnd
    }

    private function logException(\Exception $e, $evt)
    {
        if (!$this->logger) {
            // @codeCoverageIgnoreStart
            return;
            // @codeCoverageIgnoreEnd
        }
        $message = sprintf(
            'Applying event %s failed: %s: %s (uncaught exception) at %s line %s',
            var_export($evt, true),
            get_class($e),
            $e->getMessage(),
            $e->getFile(),
            $e->getLine()
        );

        $this->logger->error($message, ['exception' => $e]);
    }

    private function applySimple($evt)
    {
        switch ($evt->type) {
            case ClientBusConstants::EVENT_TYPE_CRUD:
                return $this->applyCRUD($evt);
            case ClientBusConstants::EVENT_TYPE_REQUEST:
                // @codeCoverageIgnoreStart
                return $this->applyRequest($evt);
                // @codeCoverageIgnoreEnd
            case ClientBusConstants::EVENT_TYPE_CUSTOM:
                // @codeCoverageIgnoreStart
                return $this->applyCustom($evt);
                // @codeCoverageIgnoreEnd
            default:
                throw new \Exception('invalid event type: '.json_encode($evt));
        }
    }

    /**
     * Not implemented yet.
     *
     * @codeCoverageIgnore
     *
     * @param mixed $evt
     */
    private function applyCustom($evt)
    {
        return true;
    }

    /**
     * Not implemented yet.
     *
     * @codeCoverageIgnore
     *
     * @param mixed $evt
     */
    private function applyRequest($evt)
    {
        return true;
    }

    private function applyCRUD($evt)
    {
        if (!isset($this->classMap[$evt->name]) || !$this->classMap[$evt->name]) {
            return false;
        }
        $className = $this->classMap[$evt->name];
        $repo = $this->em->getRepository($className);

        if (ClientBusConstants::EVENT_CRUD_DELETE === $evt->data->action) {
            $objects = [];
            foreach ($evt->data->data as $GUID) {
                $objects[] = $repo->find($GUID);
            }
            $objects = array_filter($objects);
            foreach ($objects as $obj) {
                $this->em->remove($obj);
            }
            $this->em->flush();

            return true;
        }
        $target = null;
        if (ClientBusConstants::EVENT_CRUD_UPDATE === $evt->data->action) {
            $target = $repo->find($evt->data->data->id);
            if (!$target) {
                return true; //skip updating nonexistent entities
            }
        } elseif (ClientBusConstants::EVENT_CRUD_CREATE === $evt->data->action) {
            $target = $repo->find($evt->data->data->id);
            if ($target) {
                return true; //skip creating existing entities
            }
        }
        $object = $this->resolver->resolveObject(
                $evt->data->data,
                $className,
                $target
        );
        $errors = $this->validator->validate($object);
        if (count($errors) > 0) {
            $this->em->clear();
            throw new \Exception('Validation failed: '.(string) $errors);
        }
        $this->em->flush();

        return true;
    }
}
