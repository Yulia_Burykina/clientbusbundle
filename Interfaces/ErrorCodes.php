<?php

namespace AveSystems\ClientBusBundle\Interfaces;

class ErrorCodes
{
    const WRITE_EXECUTION_STATE_FAILED = 21;
    const READ_COMMIT_ROLLBACK_FAILED = 22;
    const INVALID_COMMIT_ROLLBACK_MESSAGE = 23;
    const COMMIT_ROLLBACK_FAILED = 24;
    const COMMIT_ROLLBACK_NOTIFY_FAIL = 25;
}
