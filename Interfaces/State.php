<?php

namespace AveSystems\ClientBusBundle\Interfaces;

/**
 * This class describes states of application or service bus
 * that are used to communicate via socket.
 *
 * Class State
 */
class State
{
    const OK = 0;
    const ERROR = 1;
    const INSYNC = 2;
    const NOT_IMPLEMENTED = 3;
    const NOT_AUTH = 4;
    const OFFLINE = 5;
}
