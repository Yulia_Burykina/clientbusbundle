<?php

namespace AveSystems\ClientBusBundle\Interfaces;

/**
 * Socket messages interface.
 *
 * Class StateMessage
 */
class StateMessage
{
    /** @var State */
    public $status;
    /** @var string */
    public $message;
    /** @var string */
    public $relation;

    public function __construct(int $status, string $relation, string $message = null)
    {
        $this->status = $status;
        $this->relation = $relation;
        $this->message = $message;
    }
}
