<?php

namespace AveSystems\ClientBusBundle\Interfaces;

class ExecutionStep
{
    const UNSET = 0;
    const AFTER_COMMIT = 1;
    const COMMIT_SENT = 2;
}
