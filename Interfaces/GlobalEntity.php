<?php

namespace AveSystems\ClientBusBundle\Interfaces;

interface GlobalEntity
{
    public function setId($id);

    public function getId();
}
