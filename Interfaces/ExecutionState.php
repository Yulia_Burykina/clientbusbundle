<?php

namespace AveSystems\ClientBusBundle\Interfaces;

/**
 * Class ExecutionState.
 */
class ExecutionState
{
    /** @var ExecutionStep */
    public $step;

    public function __construct(int $step)
    {
        $this->step = $step;
    }
}
