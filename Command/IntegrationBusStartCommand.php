<?php

namespace AveSystems\ClientBusBundle\Command;

use AveSystems\ClientBusBundle\EventListener\GenerateEventListener;
use AveSystems\ClientBusBundle\Helper\ClientBusConstants;
use AveSystems\ClientBusBundle\Interfaces\ErrorCodes;
use AveSystems\ClientBusBundle\Interfaces\ExecutionState;
use AveSystems\ClientBusBundle\Interfaces\ExecutionStep;
use AveSystems\ClientBusBundle\Interfaces\State;
use AveSystems\ClientBusBundle\Interfaces\StateMessage;
use AveSystems\ClientBusBundle\Service\EventExecutor;
use AveSystems\ClientBusBundle\Service\EventExecutorInterface;
use AveSystems\ClientBusBundle\Service\ExecutionStateStorage;
use AveSystems\ClientBusBundle\Service\ExecutionStateStorageInterface;
use AveSystems\ClientBusBundle\Service\QueueHelper;
use AveSystems\ClientBusBundle\Service\QueueHelperInterface;
use AveSystems\ClientBusBundle\Service\SocketInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ClientBusInitCommand - command for running client bus.
 */
class IntegrationBusStartCommand extends Command
{
    const NAME = 'integration:bus:start';
    /** @var QueueHelper */
    protected $queueHelper;
    /** @var SocketInterface */
    protected $socket;
    /** @var EventExecutor */
    protected $eventExecutor;
    /** @var ExecutionStateStorage */
    protected $executionStateStorage;
    /** @var array */
    protected $client;
    /** @var string */
    protected $listenQueue;
    /** @var EntityManagerInterface */
    protected $em;
    /** @var GenerateEventListener */
    protected $listener;

    public function __construct(
        SocketInterface $socket,
        EventExecutorInterface $executor,
        ExecutionStateStorageInterface $storage,
        QueueHelperInterface $helper,
        EntityManagerInterface $em,
        GenerateEventListener $listener,
        $client,
        $listenQueue
    ) {
        $this->socket = $socket;
        $this->eventExecutor = $executor;
        $this->executionStateStorage = $storage;
        $this->queueHelper = $helper;
        $this->client = $client;
        $this->listenQueue = $listenQueue;
        $this->em = $em;
        $this->listener = $listener;
        parent::__construct();
    }

    /**
     * Processes message from queue with guaranteed delivery.
     *
     * @param $evt
     *
     * @throws \Exception
     */
    public function processEvent($evt)
    {
        /** @var ExecutionState $executionState */
        $executionState = $this->executionStateStorage->get();

        $msg = @json_decode($evt->body);
        if (!$msg) {
            return;
        }
        //TODO: must repeat the process after reviving but with predetermined result
        if (isset($executionState->step) && ExecutionStep::COMMIT_SENT === $executionState->step) {
            $this->acknowledgeEvt($evt);

            return;
        }

        if (ClientBusConstants::EVENT_TYPE_SYNC === $msg->syncType) {
            $this->syncEventCommunicate($msg, $executionState);
        } else {
            $this->asyncEventCommunicate($msg);
        }
        $this->acknowledgeEvt($evt);
    }

    public function launch()
    {
        //we don't want to fire the events that were sent from service bus back to it
        $this->listener->setEnabled(false);

        $this->socket->create();

        list($authState, $message) = $this->socket->authenticate($this->client);
        if (!$authState) {
            throw new \Exception('not authenticated: '.$message, State::NOT_AUTH);
        }

        $this->queueHelper->listen([$this, 'processEvent'], $this->listenQueue);
    }

    protected function configure()
    {
        $this->setName(self::NAME);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->launch();

            return 0;
        } catch (\Exception $ex) {
            $output->writeln($ex->getMessage());

            return 1;
        }
    }

    protected function acknowledgeEvt($evt)
    {
        $this->queueHelper->acknowledge($evt);
        $this->executionStateStorage->set(new ExecutionState(ExecutionStep::UNSET));
    }

    protected function checkActionMessage($msg)
    {
        return isset($msg->do) && in_array($msg->do, ['Commit', 'Rollback'], true);
    }

    protected function newEventOrBeforeCommit($executionState)
    {
        return !isset($executionState->step) || ExecutionStep::UNSET === $executionState->step;
    }

    protected function syncEventCommunicate($msg, $executionState)
    {
        if ($this->newEventOrBeforeCommit($executionState)) {
            //TODO: How to handle repeated execution fail?
            $success = $this->eventExecutor->applyEvent($msg, true);
        } else {
            $success = true;
        }

        $state = $success ? State::OK : State::INSYNC;

        try {
            $this->socket->write(new StateMessage($state, $msg->id));
        } catch (\Exception $ex) {
            throw new \Exception('Unable to write execution state to ESB',
                ErrorCodes::WRITE_EXECUTION_STATE_FAILED, $ex);
        }

        if (!$success) {
            throw new \Exception('Insync', State::INSYNC);
        }

        try {
            $commit = $this->socket->read();
        } catch (\Exception $ex) {
            throw new \Exception('Unable to read Commit|Rollback message from ESB',
                ErrorCodes::READ_COMMIT_ROLLBACK_FAILED, $ex);
        }

        if (!$this->checkActionMessage($commit)) {
            throw new \Exception('Invalid commit/rollback message',
                ErrorCodes::INVALID_COMMIT_ROLLBACK_MESSAGE);
        }

        // Commit stage
        if ($this->newEventOrBeforeCommit($executionState)) {
            if ('Commit' === $commit->do) {
                $success = $this->eventExecutor->commitEvent($msg);
            } else {
                $success = $this->eventExecutor->rollbackEvent($msg);
            }
        } else {
            $success = true;
        }

        if (!$success) {
            throw new \Exception('Commit or Rollback failed', ErrorCodes::COMMIT_ROLLBACK_FAILED);
        }

        $this->executionStateStorage->set(new ExecutionState(ExecutionStep::AFTER_COMMIT));

        try {
            $this->socket->write(new StateMessage(State::OK, $msg->id));
        } catch (\Exception $ex) {
            throw new \Exception('Can not notify about successful commit or rollback',
                ErrorCodes::COMMIT_ROLLBACK_NOTIFY_FAIL);
        }

        $this->executionStateStorage->set(new ExecutionState(ExecutionStep::COMMIT_SENT));
    }

    protected function asyncEventCommunicate($msg)
    {
        $success = $this->eventExecutor->applyEvent($msg);
        if (!$success) {
            $this->socket->write(new StateMessage(State::INSYNC, $msg->id ?? null));
            throw new \Exception('Insync', State::INSYNC);
        }
    }
}
