<?php

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

class AppKernel extends Kernel
{
    /**
     * @return array
     */
    public function registerBundles()
    {
        $bundles = [
            new \Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new \Symfony\Bundle\MonologBundle\MonologBundle(),
            new \Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new \Jett\JSONEntitySerializerBundle\JettJSONEntitySerializerBundle(),
            new \AveSystems\ClientBusBundle\AveClientBusBundle(),
            new \Symfony\Bundle\TwigBundle\TwigBundle(),
            new \AveSystems\ObjectResolverBundle\ObjectResolverBundle(),
        ];

        return $bundles;
    }

    /**
     * @param \Symfony\Component\Config\Loader\LoaderInterface $loader
     */
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config.yml');
    }
}
