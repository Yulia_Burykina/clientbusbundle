<?php

namespace AveSystems\ClientBusBundle\Tests\EventListener;

use AveSystems\ClientBusBundle\Helper\ClientBusConstants;
use AveSystems\ClientBusBundle\Tests\Entity\Role;
use AveSystems\ClientBusBundle\Tests\Entity\User;
use AveSystems\ClientBusBundle\Tests\QueueHelperMock;
use Doctrine\Bundle\DoctrineBundle\Command\Proxy\UpdateSchemaDoctrineCommand;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\DependencyInjection\ContainerInterface;

class GenerateEventListenerTest extends KernelTestCase
{
    /** @var QueueHelperMock */
    private $queue;

    /** @var EntityManager */
    private $em;

    /** @var ContainerInterface */
    private $container;

    private $serializer;

    private $user;

    private $role1;
    private $role2;

    public function setUp()
    {
        self::bootKernel();
        $this->container = self::$kernel->getContainer();

        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->queue = $this->container->get('ave_client_bus.queue_mock');
        $this->serializer = $this->container->get('entity_serializer');

        $app = new Application(self::$kernel);
        $app->add(new UpdateSchemaDoctrineCommand());
        $arguments = [
            'command' => 'doctrine:schema:update',
            '--force' => true,
        ];

        $input = new ArrayInput($arguments);

        $command = $app->find('doctrine:schema:update');
        $output = new BufferedOutput();
        $command->run($input, $output);
    }

    public function testEvents()
    {
        $role1 = new Role();
        $role1->setTitle('role1');
        $this->em->persist($role1);
        $role2 = new Role();
        $role2->setTitle('role2');
        $this->em->persist($role2);
        $user = new User();
        $user->setFirstName('Егор');
        $user->setLastName('Бурыкин');
        $user->setUserame('egor');
        $user->addRole($role1);
        $this->em->persist($user);
        $this->em->flush();
        $this->user = $user;
        $this->role1 = $role1;
        $this->role2 = $role2;

        $event = $this->queue->viewMessages()[0];
        $event = json_decode($event);
        $this->assertEquals(ClientBusConstants::EVENT_TYPE_TRANSACTION, $event->type);
        $roles = [$event->data[0], $event->data[1]];
        $this->assertEquals(3, count($event->data));
        foreach ($roles as $role) {
            $this->assertEquals('role', $role->name);
            $this->assertEquals(ClientBusConstants::EVENT_TYPE_CRUD, $role->type);
            $this->assertEquals(ClientBusConstants::EVENT_CRUD_CREATE, $role->data->action);
            $this->assertContains($role->data->data->title, ['role1', 'role2']);
        }
        $event2 = $event->data[2];
        $this->assertEquals('user', $event2->name);
        $this->assertEquals(ClientBusConstants::EVENT_TYPE_CRUD, $event2->type);
        $this->assertEquals(ClientBusConstants::EVENT_CRUD_CREATE, $event2->data->action);
        $this->assertEquals(1, count($event2->data->data->roles));

        $this->user->addRole($this->role2);
        $this->serializer->cleanCache();
        $this->em->flush();
        $event = $this->queue->viewMessages()[1];
        $event = json_decode($event);
        $this->assertEquals(ClientBusConstants::EVENT_TYPE_TRANSACTION, $event->type);
        $this->assertEquals(1, count($event->data));
        $this->assertEquals('user', $event->data[0]->name);
        $this->assertEquals(ClientBusConstants::EVENT_TYPE_CRUD, $event->data[0]->type);
        $this->assertEquals(ClientBusConstants::EVENT_CRUD_UPDATE, $event->data[0]->data->action);
        $this->assertEquals(2, count($event->data[0]->data->data->roles));
        foreach ($event->data[0]->data->data->roles as $role) {
            $this->assertObjectHasAttribute('id', $role);
        }

        $this->em->remove($this->user);
        $this->em->remove($this->role1);
        $this->em->remove($this->role2);
        $this->serializer->cleanCache();
        $this->em->flush();
        $event = $this->queue->viewMessages()[2];
        $event = json_decode($event);
        $this->assertEquals(ClientBusConstants::EVENT_TYPE_TRANSACTION, $event->type);
        $this->assertEquals(2, count($event->data));
        $this->assertEquals('user', $event->data[0]->name);
        $this->assertEquals(ClientBusConstants::EVENT_TYPE_CRUD, $event->data[0]->type);
        $this->assertEquals(ClientBusConstants::EVENT_CRUD_DELETE, $event->data[0]->data->action);
        $this->assertEquals(1, count($event->data[0]->data->data));
        $this->assertInternalType('string', $event->data[0]->data->data[0]);
        $this->assertEquals('role', $event->data[1]->name);
        $this->assertEquals(ClientBusConstants::EVENT_TYPE_CRUD, $event->data[1]->type);
        $this->assertEquals(ClientBusConstants::EVENT_CRUD_DELETE, $event->data[1]->data->action);
        $this->assertEquals(2, count($event->data[1]->data->data));
        foreach ($event->data[1]->data->data as $id) {
            $this->assertInternalType('string', $id);
        }
    }
}
