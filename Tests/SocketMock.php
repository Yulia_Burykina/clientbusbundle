<?php

namespace AveSystems\ClientBusBundle\Tests;

use AveSystems\ClientBusBundle\Interfaces\State;
use AveSystems\ClientBusBundle\Interfaces\StateMessage;
use AveSystems\ClientBusBundle\Service\SocketInterface;

class SocketMock implements SocketInterface
{
    private $protocol;

    private $curPos = 0;

    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
    }

    public function getProtocol()
    {
        return $this->protocol;
    }

    public function create()
    {
        $this->curPos = 0;
        if (empty($this->protocol)) {
            throw new \Exception('Set protocol for test socket server');
        }
    }

    public function authenticate($credentials = null): array
    {
        $this->write($credentials);
        /** @var StateMessage $resp */
        $resp = $this->read();

        if (!isset($resp->status) || !is_numeric($resp->status)) {
            return [false, 'Invalid response object from ESB service.'];
        }
        $state = (int) $resp->status;
        if (State::OK !== $state) {
            return [false, $state];
        }

        return [true, null];
    }

    public function write($msg)
    {
        $message = $this->protocol[$this->curPos];
        if ('write_fail' === $message) {
            throw new \Exception('Unable to write to socket');
        }

        $this->protocol[$this->curPos] = $msg;
        ++$this->curPos;
    }

    public function read()
    {
        $message = $this->protocol[$this->curPos];
        if ('read_fail' === $message) {
            throw new \Exception('Unable to read from socket');
        }
        ++$this->curPos;

        return $message;
    }

    public function sendBack($msg)
    {
        $this->write($msg);

        return $this->read();
    }

    public function authSendBack($credentials, $msg)
    {
        $this->authenticate($credentials);

        return $this->sendBack($msg);
    }
}
