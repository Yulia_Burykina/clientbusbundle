<?php

namespace AveSystems\ClientBusBundle\Tests;

use AveSystems\ClientBusBundle\Interfaces\ExecutionState;
use AveSystems\ClientBusBundle\Service\ExecutionStateStorageInterface;

class ExecutionStateStorageMock implements ExecutionStateStorageInterface
{
    private $state;

    public function get()
    {
        return $this->state;
    }

    public function set(ExecutionState $state = null)
    {
        $this->state = $state;
    }
}
