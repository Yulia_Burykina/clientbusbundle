<?php

namespace AveSystems\ClientBusBundle\Tests\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="first_name", type="string", length=45)
     */
    protected $firstName;

    /**
     * Фамилия.
     *
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    protected $lastName;

    /**
     * Отчество.
     *
     * @var string
     * @ORM\Column(type="string")
     */
    protected $userame;

    /**
     * @ORM\ManyToMany(targetEntity="AveSystems\ClientBusBundle\Tests\Entity\Role")
     */
    protected $roles;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getUserame(): string
    {
        return $this->userame;
    }

    /**
     * @param string $userame
     */
    public function setUserame(string $userame)
    {
        $this->userame = $userame;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param mixed $role
     */
    public function addRole($role)
    {
        $this->roles->add($role);
    }

    public function removeRole($role)
    {
        $this->roles->remove($role);
    }
}
