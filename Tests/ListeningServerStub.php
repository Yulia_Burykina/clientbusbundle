<?php

namespace AveSystems\ClientBusBundle\Tests;

require_once 'CommunicationProtocolConsts.php';
require_once __DIR__.'/../Interfaces/State.php';

class ListeningServerStub
{
    const SEP = "\r\n";
    private $socket;
    private $address;
    private $port;

    private $errno = 0;
    private $errstr = '';

    private $connected = false;

    public function __construct($address, $port)
    {
        $this->address = $address;
        $this->port = $port;
    }

    public function __destruct()
    {
        $this->stop();
    }

    /**
     * Launches server.
     *
     * @throws \Exception
     */
    public function listen()
    {
        $this->socket = stream_socket_server(
            'tcp://'.$this->address.':'.$this->port,
            $this->errno,
            $this->errstr
        );
        if (false === $this->socket) {
            throw new \Exception(
                sprintf('failed to create socket: %s', $this->errstr)
            );
        }
        $this->connected = true;
        while ($this->connected) {
            //blocking call for 1 second
            $connection = @stream_socket_accept($this->socket, 1);
            if (false === $connection) {
                //timeout happened; checking for signals to terminate;
                pcntl_signal_dispatch();
                //if there won't be any, just restart accepting
                continue;
            } elseif ($connection > 0) {
                $this->communicate($connection);
            } else {
                throw new \Exception('server socket accept error');
            }
        }
    }

    public function stop()
    {
        if ($this->connected) {
            fclose($this->socket);
            $this->connected = false;
        }
    }

    private function communicate($connection)
    {
        while ($this->connected) {
            $message = $this->read($connection);

            if ('Client disconnected' === $message) {
                return;
            }
            if (false === $message) {
                throw new \Exception('server socket error while reading');
            }
            $key = array_search($message, CommunicationProtocolConsts::REQUESTS, true);
            if (false === $key) {
                throw new \Exception('undefined message');
            }
            $response = CommunicationProtocolConsts::RESPONSES[$key];
            if (!$this->write($connection, $response)) {
                throw new \Exception('server socket error while writing');
            }
        }
    }

    /**
     * Writes message according to application protocol.
     *
     * @param $connection
     * @param $message
     *
     * @return bool
     */
    private function write($connection, $message)
    {
        $len = strlen($message);
        $msg = 'Length: '.$len.self::SEP;
        $msg .= self::SEP;
        $msg .= $message.self::SEP;
        $msgLen = strlen($msg);

        return $msgLen === @fwrite($connection, $msg);
    }

    /**
     * Reads from socket.
     *
     * @param $connection
     *
     * @return string|bool
     */
    private function read($connection)
    {
        $buf = '';
        $length = 0;
        $isContent = false;
        while ($this->connected) {
            if (!$isContent) {
                $string = fgets($connection, 1024);
                if (!$string) {
                    return 'Client disconnected';
                }
                if (preg_match("/Length:\s(\d+)/", $string, $matches)) {
                    $length = (int) $matches[1];
                }
                $buf .= $string;
                if (substr($buf, -4) === self::SEP.self::SEP) {
                    $isContent = true;
                }
            } else {
                $content = fread($connection, $length + strlen(self::SEP));
                if ($content && (strlen($content) - strlen(self::SEP)) === $length) {
                    return trim($content);
                }

                return false;
            }
        }

        return true;
    }
}
