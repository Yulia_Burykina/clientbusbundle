<?php

namespace AveSystems\ClientBusBundle\Tests\Command;

use AveSystems\ClientBusBundle\Command\IntegrationBusStartCommand;
use AveSystems\ClientBusBundle\EventListener\GenerateEventListener;
use AveSystems\ClientBusBundle\Helper\ClientBusConstants;
use AveSystems\ClientBusBundle\Interfaces\ErrorCodes;
use AveSystems\ClientBusBundle\Interfaces\ExecutionStep;
use AveSystems\ClientBusBundle\Interfaces\State;
use AveSystems\ClientBusBundle\Tests\Entity\Role;
use AveSystems\ClientBusBundle\Tests\EventExecutorMock;
use AveSystems\ClientBusBundle\Tests\ExecutionStateStorageMock;
use AveSystems\ClientBusBundle\Tests\QueueHelperMock;
use AveSystems\ClientBusBundle\Tests\SocketMock;
use Doctrine\Bundle\DoctrineBundle\Command\Proxy\UpdateSchemaDoctrineCommand;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;

class IntegrationBusStartCommandTest extends KernelTestCase
{
    /** @var SocketMock $socket */
    private $socket;
    /** @var EventExecutorMock $executor */
    private $executor;
    /** @var ExecutionStateStorageMock $storage */
    private $storage;
    /** @var QueueHelperMock $queue */
    private $queue;
    /** @var IntegrationBusStartCommand */
    private $command;
    /** @var EntityManagerInterface */
    private $em;
    /** @var GenerateEventListener */
    private $listener;

    public function setUp()
    {
        self::bootKernel();

        $this->socket = $this->get('ave_client_bus.socket_mock');

        $this->executor = $this->get('ave_client_bus.executor_mock');

        $this->storage = $this->get('ave_client_bus.execution_state_storage_mock');

        $this->queue = $this->get('ave_client_bus.queue_mock');

        $this->em = $this->get('doctrine.orm.entity_manager');

        $this->listener = $this->get('ave_client_bus.generate_event_listener');

        $this->command = new IntegrationBusStartCommand(
            $this->socket,
            $this->executor,
            $this->storage,
            $this->queue,
            $this->em,
            $this->listener,
            [],
            '');
    }

    public function get($name)
    {
        return self::$kernel->getContainer()->get($name);
    }

    public function getAsyncEvent()
    {
        return (object) [
            'body' => json_encode((object) [
                'syncType' => ClientBusConstants::EVENT_TYPE_ASYNC,
                'id' => 1,
                'test' => 1,
            ]),
        ];
    }

    public function getSyncEvent()
    {
        return (object) [
            'body' => json_encode((object) [
                'syncType' => ClientBusConstants::EVENT_TYPE_SYNC,
                'id' => 1,
                'test' => 1,
            ]),
        ];
    }

    public function testAuthFail()
    {
        $protocol = [
            'write',
            (object) ['status' => State::NOT_AUTH],
        ];
        $this->socket->setProtocol($protocol);
        try {
            $this->command->launch();
        } catch (\Exception $ex) {
            $this->assertEquals(State::NOT_AUTH, $ex->getCode());
        }
    }

    public function testAsyncFailEvent()
    {
        $protocol = [
            'write',
            (object) ['status' => State::OK],
            'write',
        ];
        $this->socket->setProtocol($protocol);
        $this->executor->configure(false, false, false);
        $this->queue->addMessage($this->getAsyncEvent());
        try {
            $this->command->launch();
        } catch (\Exception $ex) {
            $this->assertEquals(State::INSYNC, $ex->getCode());
        }
    }

    public function testAsyncSuccessEvent()
    {
        $protocol = [
            'write',
            (object) ['status' => State::OK],
        ];
        $this->socket->setProtocol($protocol);
        $this->executor->configure();
        $this->queue->addMessage($this->getAsyncEvent());
        try {
            $this->command->launch();
        } catch (\Exception $ex) {
            $this->assertEquals(State::OK, $ex->getCode());
        }
    }

    public function testSyncSuccessEvent()
    {
        $protocol = [
            'write',
            (object) ['status' => State::OK],
            'write',
            (object) ['do' => 'Commit'],
            'write',
        ];
        $this->socket->setProtocol($protocol);
        $this->executor->configure();
        $this->queue->addMessage($this->getSyncEvent());
        try {
            $this->command->launch();
        } catch (\Exception $ex) {
            $this->assertEquals(State::OK, $ex->getCode());
        }
    }

    public function testSyncEventUnableToSendExecutionStatus()
    {
        $protocol = [
            'write',
            (object) ['status' => State::OK],
            'write_fail',
            (object) ['do' => 'Commit'],
            'write',
        ];
        $this->socket->setProtocol($protocol);
        $this->executor->configure();
        $this->queue->addMessage($this->getSyncEvent());
        try {
            $this->command->launch();
        } catch (\Exception $ex) {
            $this->assertEquals(ErrorCodes::WRITE_EXECUTION_STATE_FAILED, $ex->getCode());
        }
    }

    public function testSyncEventUnableToReadCommitRollbackMessage()
    {
        $protocol = [
            'write',
            (object) ['status' => State::OK],
            'write',
            'read_fail',
            'write',
        ];
        $this->socket->setProtocol($protocol);
        $this->executor->configure();
        $this->queue->addMessage($this->getSyncEvent());
        try {
            $this->command->launch();
        } catch (\Exception $ex) {
            $this->assertEquals(ErrorCodes::READ_COMMIT_ROLLBACK_FAILED, $ex->getCode());
        }
    }

    public function testSyncEventUnableToCommitOrRollback()
    {
        $protocol = [
            'write',
            (object) ['status' => State::OK],
            'write',
            (object) ['do' => 'Commit'],
            'write',
        ];
        $this->socket->setProtocol($protocol);
        $this->executor->configure(true, false, false);
        $this->queue->addMessage($this->getSyncEvent());
        try {
            $this->command->launch();
        } catch (\Exception $ex) {
            $this->assertEquals(ErrorCodes::COMMIT_ROLLBACK_FAILED, $ex->getCode());
            $this->assertNull($this->storage->get());
        }
    }

    public function testSyncEventRecoverEventFromAfterCommit()
    {
        $protocol = [
            'write',
            (object) ['status' => State::OK],
            'write',
            (object) ['do' => 'Commit'],
            'write_fail',
        ];
        $this->socket->setProtocol($protocol);
        $this->executor->configure();
        $this->queue->addMessage($this->getSyncEvent());
        try {
            $this->command->launch();
        } catch (\Exception $ex) {
            $this->assertEquals(ErrorCodes::COMMIT_ROLLBACK_NOTIFY_FAIL, $ex->getCode());
            $this->assertEquals(ExecutionStep::AFTER_COMMIT, $this->storage->get()->step);
            $this->assertTrue($this->executor->commitHasBeenDone());
        }
        $protocol = [
            'write',
            (object) ['status' => State::OK],
            'write',
            (object) ['do' => 'Commit'],
            'write',
        ];
        $this->socket->setProtocol($protocol);
        $this->executor->configure();
        try {
            $this->command->launch();
            $this->assertFalse($this->executor->commitHasBeenDone());
        } catch (\Exception $ex) {
            $this->assertTrue(false);
        }
    }

    public function testGenerateEventListenerIsDisabled()
    {
        $this->updateDatabase();

        $this->socket->setProtocol(null);
        try {
            $this->command->launch();
        } catch (\Exception $ex) {
        }

        $role1 = new Role();
        $role1->setTitle('role1');
        $this->em->persist($role1);
        $this->em->flush();
        $event = $this->queue->viewMessages();
        $this->assertEquals([], $event);
    }

    protected function updateDatabase()
    {
        $app = new Application(self::$kernel);
        $app->add(new UpdateSchemaDoctrineCommand());
        $arguments = [
            'command' => 'doctrine:schema:update',
            '--force' => true,
        ];

        $input = new ArrayInput($arguments);

        $command = $app->find('doctrine:schema:update');
        $output = new BufferedOutput();
        $command->run($input, $output);
    }
}
