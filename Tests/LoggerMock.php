<?php

namespace AveSystems\ClientBusBundle\Tests;

use Psr\Log\LoggerInterface;

class LoggerMock implements LoggerInterface
{
    private $messages = [
        'emergency' => [],
        'alert' => [],
        'critical' => [],
        'error' => [],
        'warning' => [],
        'notice' => [],
        'info' => [],
        'debug' => [],
    ];

    public function getMessages()
    {
        return $this->messages;
    }

    public function emergency($message, array $context = [])
    {
        $this->messages['emergency'][] = $message;
    }

    public function alert($message, array $context = [])
    {
        $this->messages['alert'][] = $message;
    }

    public function critical($message, array $context = [])
    {
        $this->messages['critical'][] = $message;
    }

    public function error($message, array $context = [])
    {
        $this->messages['error'][] = $message;
    }

    public function warning($message, array $context = [])
    {
        $this->messages['warning'][] = $message;
    }

    public function notice($message, array $context = [])
    {
        $this->messages['notice'][] = $message;
    }

    public function info($message, array $context = [])
    {
        $this->messages['info'][] = $message;
    }

    public function debug($message, array $context = [])
    {
        $this->messages['debug'][] = $message;
    }

    public function log($level, $message, array $context = [])
    {
        // TODO: Implement log() method.
    }
}
