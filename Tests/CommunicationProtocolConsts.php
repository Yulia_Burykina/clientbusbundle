<?php

namespace AveSystems\ClientBusBundle\Tests;

use AveSystems\ClientBusBundle\Interfaces\State;

class CommunicationProtocolConsts
{
    const REQUESTS = [
        '{"random_id":"test","secret":"test","name":"test"}',
        '{"random_id":"test","secret":"wrong_test","name":"test"}',
        '{"message":"test_auth_send_back"}',
        '{"message":"test_send_back"}',
    ];

    const RESPONSES = [
        '{"status":'.State::OK.'}',
        '{"status":'.State::NOT_AUTH.'}',
        '{"message":"test_auth_send_back_ok"}',
        '{"message":"test_send_back_ok"}',
    ];
}
