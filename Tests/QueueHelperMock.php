<?php

namespace AveSystems\ClientBusBundle\Tests;

use AveSystems\ClientBusBundle\Service\QueueHelperInterface;

class QueueHelperMock implements QueueHelperInterface
{
    private $messages = [];

    public function addMessage($message)
    {
        $this->messages[] = $message;
    }

    public function listen(callable $callback, $queue)
    {
        if (empty($this->messages)) {
            return false;
        }
        $callback(end($this->messages));
    }

    public function acknowledge($msg)
    {
        array_pop($this->messages);
    }

    public function send($msg, $queue)
    {
        $this->addMessage($msg);
    }

    public function viewMessages()
    {
        return $this->messages;
    }
}
