<?php

namespace AveSystems\ClientBusBundle\Tests\EventListener;

use AveSystems\ClientBusBundle\Service\Socket;
use AveSystems\ClientBusBundle\Tests\CommunicationProtocolConsts;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SocketTest extends KernelTestCase
{
    /** @var Socket */
    private $socket;

    private $serverPids = [];

    /** @var ContainerInterface */
    private $container;

    public function setUp()
    {
        self::bootKernel();
        $this->container = self::$kernel->getContainer();
        $address = $this->getParameter('ave_client_bus.service_bus_address');
        $port = $this->getParameter('ave_client_bus.service_bus_port');

        $pid = exec('php '.__DIR__."/../ServerStubCommand.php $address $port > /dev/null 2>&1 & echo $!");

        //if we skip this, server won't launch in time for client to connect,
        //and tests will crush
        usleep(50000);
        $this->serverPids[] = $pid;

        $this->socket = $this->get('ave_client_bus.test_socket');
    }

    public function tearDown()
    {
        $this->socket->stop();
        $this->serverPids = array_filter($this->serverPids);
        foreach ($this->serverPids as $pid) {
            posix_kill($pid, SIGINT);
        }
    }

    public function get($name)
    {
        return $this->container->get($name);
    }

    public function getParameter($name)
    {
        return $this->container->getParameter($name);
    }

    public function testCreate()
    {
        $this->socket->create();
        $this->assertTrue(true);
    }

    public function testAuthenticateSuccess()
    {
        $this->socket->create();
        list($success, $error) = $this->socket->authenticate(
            json_decode(CommunicationProtocolConsts::REQUESTS[0])
        );
        $this->assertTrue($success);
        $this->assertNull($error);
    }

    public function testAuthenticateEmptyCredentials()
    {
        $this->socket->create();
        $this->expectException(\InvalidArgumentException::class);
        $this->socket->authenticate();
    }

    public function testAuthenticateWrongCredentials()
    {
        $this->socket->create();
        list($success, $error) = $this->socket->authenticate(
            json_decode(CommunicationProtocolConsts::REQUESTS[1])
        );
        $this->assertFalse($success);
        $this->assertEquals('wrong credentials', $error);
    }

    public function testAuthSendBack()
    {
        $this->socket->create();
        $msg = $this->socket->sendBack(
            json_decode(CommunicationProtocolConsts::REQUESTS[3])
        );
        $this->assertEquals(json_encode($msg), CommunicationProtocolConsts::RESPONSES[3]);
    }

    public function testSendBack()
    {
        $this->socket->create();
        $msg = $this->socket->authSendBack(
            json_decode(CommunicationProtocolConsts::REQUESTS[0]),
            json_decode(CommunicationProtocolConsts::REQUESTS[2])
        );
        $this->assertEquals(json_encode($msg), CommunicationProtocolConsts::RESPONSES[2]);
    }
}
