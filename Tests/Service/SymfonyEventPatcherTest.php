<?php

namespace AveSystems\ClientBusBundle\Tests\Service;

use AveSystems\ClientBusBundle\Helper\ClientBusConstants;
use AveSystems\ClientBusBundle\Service\SymfonyEventPatcher;
use AveSystems\ClientBusBundle\Tests\Entity\Role;
use Doctrine\Bundle\DoctrineBundle\Command\Proxy\UpdateSchemaDoctrineCommand;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SymfonyEventPatcherTest extends KernelTestCase
{
    /** @var SymfonyEventPatcher */
    private $patcher;

    /** @var EntityManager */
    private $em;

    /** @var ContainerInterface */
    private $container;

    private $logger;

    public function setUp()
    {
        self::bootKernel();
        $this->container = self::$kernel->getContainer();

        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->patcher = $this->container->get('ave_client_bus.symfony_patcher');
        $this->logger = $this->container->get('ave_client_bus.logger_mock');

        $app = new Application(self::$kernel);
        $app->add(new UpdateSchemaDoctrineCommand());
        $arguments = [
            'command' => 'doctrine:schema:update',
            '--force' => true,
        ];

        $input = new ArrayInput($arguments);

        $command = $app->find('doctrine:schema:update');
        $output = new BufferedOutput();
        $command->run($input, $output);
    }

    public function testApplySimpleAsTransactionAndCommit()
    {
        $event = new \stdClass();
        $event->type = ClientBusConstants::EVENT_TYPE_CRUD;
        $event->name = 'role';
        $event->data = new \stdClass();
        $event->data->action = ClientBusConstants::EVENT_CRUD_CREATE;
        $event->data->data = new \stdClass();
        $event->data->data->id = 1;
        $event->data->data->title = 'test_1';

        $res = $this->patcher->apply($event, true);
        $this->assertTrue($res);
        $resCommit = $this->patcher->commit($event);
        $this->assertTrue($resCommit);

        $this->em->clear();
        $role = $this->em->find(Role::class, 1);
        $this->assertNotNull($role);
        $this->assertEquals('test_1', $role->getTitle());
    }

    public function testApplySimpleAsTransactionAndRollback()
    {
        $event = new \stdClass();
        $event->type = ClientBusConstants::EVENT_TYPE_CRUD;
        $event->name = 'role';
        $event->data = new \stdClass();
        $event->data->action = ClientBusConstants::EVENT_CRUD_CREATE;
        $event->data->data = new \stdClass();
        $event->data->data->id = 2;
        $event->data->data->title = 'test_2';

        $res = $this->patcher->apply($event, true);
        $this->assertTrue($res);
        $role = $this->em->find(Role::class, 2);
        $this->assertNotNull($role);
        $this->assertEquals('test_2', $role->getTitle());

        $resCommit = $this->patcher->rollback($event);
        $this->assertTrue($resCommit);

        $this->em->clear();
        $role = $this->em->find(Role::class, 2);
        $this->assertNull($role);
    }

    public function testApplyTransactionAsTransactionAndCommit()
    {
        $event = new \stdClass();
        $event->type = ClientBusConstants::EVENT_TYPE_TRANSACTION;
        $event->data = [];
        $e1 = new \stdClass();
        $e1->name = 'role';
        $e1->type = ClientBusConstants::EVENT_TYPE_CRUD;
        $e1->data = new \stdClass();
        $e1->data->action = ClientBusConstants::EVENT_CRUD_CREATE;
        $e1->data->data = new \stdClass();
        $e1->data->data->id = 3;
        $e1->data->data->title = 'test_3';
        $event->data[] = $e1;

        $res = $this->patcher->apply($event, true);
        $this->assertTrue($res);

        $resCommit = $this->patcher->commit($event);
        $this->assertTrue($resCommit);

        $this->em->clear();
        $role = $this->em->find(Role::class, 3);
        $this->assertNotNull($role);
        $this->assertEquals('test_3', $role->getTitle());
    }

    public function testApplySimpleDeleteSuccessfully()
    {
        $role = new Role();
        $role->setId(4);
        $role->setTitle('test_4');
        $this->em->persist($role);
        $this->em->flush();

        $this->em->clear();
        $role = $this->em->find(Role::class, 4);
        $this->assertNotNull($role);
        $this->assertEquals('test_4', $role->getTitle());

        $event = new \stdClass();
        $event->type = ClientBusConstants::EVENT_TYPE_CRUD;
        $event->name = 'role';
        $event->data = new \stdClass();
        $event->data->action = ClientBusConstants::EVENT_CRUD_DELETE;
        $event->data->data = [4];

        $res = $this->patcher->apply($event, false);
        $this->assertTrue($res);

        $this->em->clear();
        $role = $this->em->find(Role::class, 4);
        $this->assertNull($role);
    }

    public function testApplySimpleUpdateNonexistentEntity()
    {
        $event = new \stdClass();
        $event->type = ClientBusConstants::EVENT_TYPE_CRUD;
        $event->name = 'role';
        $event->data = new \stdClass();
        $event->data->action = ClientBusConstants::EVENT_CRUD_UPDATE;
        $event->data->data = new \stdClass();
        $event->data->data->id = 5;
        $event->data->data->title = 'test_5_upd';

        $res = $this->patcher->apply($event, false);
        $this->assertTrue($res);

        $this->em->clear();
        $role = $this->em->find(Role::class, 4);
        $this->assertNull($role);
    }

    public function testApplySimpleCreateExistentEntity()
    {
        $role = new Role();
        $role->setId(6);
        $role->setTitle('test_6');
        $this->em->persist($role);
        $this->em->flush();

        $event = new \stdClass();
        $event->type = ClientBusConstants::EVENT_TYPE_CRUD;
        $event->name = 'role';
        $event->data = new \stdClass();
        $event->data->action = ClientBusConstants::EVENT_CRUD_CREATE;
        $event->data->data = new \stdClass();
        $event->data->data->id = 6;
        $event->data->data->title = 'test_6_upd';

        $res = $this->patcher->apply($event, false);
        $this->assertTrue($res);

        $this->em->clear();
        $role = $this->em->find(Role::class, 6);
        $this->assertNotNull($role);
        $this->assertEquals('test_6', $role->getTitle());
    }

    public function testApplySimpleUpdateWithValidationErrors()
    {
        $role = new Role();
        $role->setId(7);
        $role->setTitle('test_7');
        $this->em->persist($role);
        $this->em->flush();

        $event = new \stdClass();
        $event->type = ClientBusConstants::EVENT_TYPE_CRUD;
        $event->name = 'role';
        $event->data = new \stdClass();
        $event->data->action = ClientBusConstants::EVENT_CRUD_UPDATE;
        $event->data->data = new \stdClass();
        $event->data->data->id = 7;
        $event->data->data->title = '';

        $res = $this->patcher->apply($event, false);
        $this->assertFalse($res);
        $log = $this->logger->getMessages();
        $this->assertContains('Validation failed', $log['error'][0]);

        $this->em->clear();
        $role = $this->em->find(Role::class, 7);
        $this->assertNotNull($role);
        $this->assertEquals('test_7', $role->getTitle());
    }

    public function testApplySimpleUpdateSuccessfully()
    {
        $role = new Role();
        $role->setId(8);
        $role->setTitle('test_8');
        $this->em->persist($role);
        $this->em->flush();

        $event = new \stdClass();
        $event->type = ClientBusConstants::EVENT_TYPE_CRUD;
        $event->name = 'role';
        $event->data = new \stdClass();
        $event->data->action = ClientBusConstants::EVENT_CRUD_UPDATE;
        $event->data->data = new \stdClass();
        $event->data->data->id = 8;
        $event->data->data->title = 'test_8_upd';

        $res = $this->patcher->apply($event, false);
        $this->assertTrue($res);

        $this->em->clear();
        $role = $this->em->find(Role::class, 8);
        $this->assertNotNull($role);
        $this->assertEquals('test_8_upd', $role->getTitle());
    }

    public function testApplySimpleCreateSuccessfully()
    {
        $event = new \stdClass();
        $event->type = ClientBusConstants::EVENT_TYPE_CRUD;
        $event->name = 'role';
        $event->data = new \stdClass();
        $event->data->action = ClientBusConstants::EVENT_CRUD_CREATE;
        $event->data->data = new \stdClass();
        $event->data->data->id = 9;
        $event->data->data->title = 'test_9';

        $res = $this->patcher->apply($event, false);
        $this->assertTrue($res);

        $this->em->clear();
        $role = $this->em->find(Role::class, 9);
        $this->assertNotNull($role);
        $this->assertEquals('test_9', $role->getTitle());
    }

    public function testApplyTransactionSuccessfully()
    {
        $event = new \stdClass();
        $event->type = ClientBusConstants::EVENT_TYPE_TRANSACTION;
        $event->data = [];
        $e1 = new \stdClass();
        $e1->name = 'role';
        $e1->type = ClientBusConstants::EVENT_TYPE_CRUD;
        $e1->data = new \stdClass();
        $e1->data->action = ClientBusConstants::EVENT_CRUD_CREATE;
        $e1->data->data = new \stdClass();
        $e1->data->data->id = 10;
        $e1->data->data->title = 'test_10';
        $event->data[] = $e1;

        $res = $this->patcher->apply($event, false);
        $this->assertTrue($res);

        $this->em->clear();
        $role = $this->em->find(Role::class, 10);
        $this->assertNotNull($role);
        $this->assertEquals('test_10', $role->getTitle());
    }

    public function testApplyTransactionAndRollbackOnFalseResult()
    {
        $event = new \stdClass();
        $event->type = ClientBusConstants::EVENT_TYPE_TRANSACTION;
        $event->data = [];
        $e1 = new \stdClass();
        $e1->name = 'role';
        $e1->type = ClientBusConstants::EVENT_TYPE_CRUD;
        $e1->data = new \stdClass();
        $e1->data->action = ClientBusConstants::EVENT_CRUD_CREATE;
        $e1->data->data = new \stdClass();
        $e1->data->data->id = 11;
        $e1->data->data->title = 'test_11';
        $event->data[] = $e1;

        $e2 = new \stdClass();
        $e2->name = 'false_name';
        $e2->type = ClientBusConstants::EVENT_TYPE_CRUD;
        $e2->data = new \stdClass();
        $e2->data->action = ClientBusConstants::EVENT_CRUD_CREATE;
        $e2->data->data = new \stdClass();
        $e2->data->data->id = 12;
        $e2->data->data->title = 'test_12';
        $event->data[] = $e2;

        $res = $this->patcher->apply($event, false);
        $this->assertFalse($res);

        $this->em->clear();
        $role = $this->em->find(Role::class, 11);
        $this->assertNull($role);
    }

    public function testApplyTransactionAndRollbackOnException()
    {
        $event = new \stdClass();
        $event->type = ClientBusConstants::EVENT_TYPE_TRANSACTION;
        $event->data = [];
        $e1 = new \stdClass();
        $e1->name = 'role';
        $e1->type = ClientBusConstants::EVENT_TYPE_CRUD;
        $e1->data = new \stdClass();
        $e1->data->action = ClientBusConstants::EVENT_CRUD_CREATE;
        $e1->data->data = new \stdClass();
        $e1->data->data->id = 13;
        $e1->data->data->title = 'test_13';
        $event->data[] = $e1;

        $e2 = new \stdClass();
        $e2->name = 'role';
        $e2->type = 9999; //invalid type
        $e2->data = new \stdClass();
        $e2->data->action = ClientBusConstants::EVENT_CRUD_CREATE;
        $e2->data->data = new \stdClass();
        $e2->data->data->id = 14;
        $e2->data->data->title = 'test_14';
        $event->data[] = $e2;

        $res = $this->patcher->apply($event, false);
        $this->assertFalse($res);

        $this->em->clear();
        $role = $this->em->find(Role::class, 13);
        $this->assertNull($role);
        $role = $this->em->find(Role::class, 14);
        $this->assertNull($role);
    }
}
