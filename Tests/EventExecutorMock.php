<?php

namespace AveSystems\ClientBusBundle\Tests;

use AveSystems\ClientBusBundle\Service\EventExecutorInterface;

class EventExecutorMock implements EventExecutorInterface
{
    private $apply = true;

    private $commit = true;

    private $rollback = true;

    private $hasBeenCommited = false;

    public function configure($apply = true, $commit = true, $rollback = true)
    {
        $this->apply = $apply;
        $this->commit = $commit;
        $this->rollback = $rollback;
        $this->hasBeenCommited = false;
    }

    public function commitHasBeenDone()
    {
        return $this->hasBeenCommited;
    }

    public function applyEvent($evt, $asTransaction = false)
    {
        return $this->apply;
    }

    public function commitEvent($evt)
    {
        $this->hasBeenCommited = true;

        return $this->commit;
    }

    public function rollbackEvent($evt)
    {
        return $this->rollback;
    }
}
