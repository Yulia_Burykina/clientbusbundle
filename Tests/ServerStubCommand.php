<?php

require_once 'ListeningServerStub.php';

$server = new \AveSystems\ClientBusBundle\Tests\ListeningServerStub($argv[1], $argv[2]);
$stop = function () use ($server) {
    $server->stop();
};
pcntl_signal(SIGTERM, $stop);
pcntl_signal(SIGINT, $stop);
$server->listen();
